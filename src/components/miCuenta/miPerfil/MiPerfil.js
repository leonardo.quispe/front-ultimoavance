import React from 'react';
import {Nav, Tab, Col, Row} from 'react-bootstrap';
import './../../../assets/styles/nav-vertical.css';
import logolat from './../../../assets/img/logolat.png';
import logoPerfil from './../../../assets/img/logoPerfil.png';
import './../../../assets/styles/divs.css';
import $ from 'jquery';
window.jQuery = $;

class MiPerfil extends React.Component {
    componentDidMount() {
        $('#mostrar-nav').on('click',function(){
            $('.nav-lat').toggleClass('mostrar');
        });
        $('#cerrar-nav').on('click',function(){
            $('.nav-lat').toggleClass('mostrar');
        })
    }
    
    render() {
        return (
            <div>
                <div id="mostrar-nav" class="body"></div>
                    <nav class="nav-lat">
                    <div id="cerrar-nav"></div>
                    <div>
                        <img src={logolat} class="logo"></img>
                    </div>
                        <ul class="menu">
                            <li><a href="/#/home"><b>inicio</b></a></li>
                            <li><a href="/#/fichas"><b>Ficha Tecnica</b></a></li>
                            <li><a href="#"><b>Servicios</b></a></li>
                        </ul>
                    </nav>
                <Tab.Container id="left-tabs-example" defaultActiveKey="first">
                    <Row>
                        <Col sm={4} style={{ backgroundColor:'#A46F1F'}}>
                            <Nav id="navOps" variant="pills" className="flex-column container"><br/>
                                <Nav.Item >
                                    <Nav.Link style={{color:'#ffffff'}} eventKey="first">Mi Perfil</Nav.Link>
                                </Nav.Item><br/>

                                <Nav.Item>
                                    <Nav.Link style={{color:'#ffffff'}} eventKey="second">Mis Ajustes</Nav.Link>
                                </Nav.Item>
                            </Nav>
                        </Col>
                        <Col sm={8} style={{ backgroundColor:'#A46F1F'}} >
                            <Tab.Content>
                                <Tab.Pane eventKey="first">
                                    <div class="container"><br/>
                                        <div id="divPerfil" class="" style={{ backgroundColor:'#E5E5E5'}}>
                                            <div id="divLogo" style={{color:'#ffffff' }} class="text-center"><br/>
                                                <div id="profileimage">
                                                    <img src={logoPerfil} class="logoPerfil"></img>
                                                </div>
                                                <h2>Carlos Eduardo</h2>
                                                <div  class="row container text-center">
                                                    <p id="p1" class="col-4">Administrador</p>
                                                    <p id="p2" class="col-1 text-center">-</p>
                                                    <p id="p3" class="col-3">Global</p>
                                                </div><br/><br/>
                                            </div><br/>

                                            <div class="container">
                                                <div class="row">
                                                    <h3>Datos Personales</h3>
                                                        <div class="col"><br/>
                                                            <div class="info-data">
                                                                <p><b>Nombre</b></p>
                                                                <div>Carlos Eduardo Pumayalla Angeles</div>
                                                            </div><br/>                       
                                                            <div class="info-data">
                                                                <p><b>Cumpleaños</b></p>
                                                                <div>1999-05-23</div>
                                                            </div>
                                                        </div>

                                                        <div class="col"><br/>
                                                            <div class="info-data">
                                                                <p><b>Departamento</b></p>
                                                                <div>Global</div>
                                                            </div><br/>
                                                            <div class="info-data">
                                                                <p><b>DNI</b></p>
                                                                <div>75214874</div>
                                                            </div>
                                                        </div>
                                                </div><br/><br/>

                                                <div class="row">
                                                    <h3>Contacto</h3><br/>
                                                        <div class="col"><br/>
                                                            <div class="info-data">
                                                                <p><b>Correo</b></p>
                                                                <div>carlos@citeccall.com</div>
                                                            </div><br/>
                                                            <div class="info-data">
                                                                <p><b>Telefono</b></p>
                                                                <div>5412354</div>
                                                            </div>
                                                        </div>

                                                        <div class="col"><br/>
                                                            <div class="info-data">
                                                                <p><b>Celular</b></p>
                                                                <div>987415421</div>
                                                            </div>
                                                        </div>                                              
                                                </div><br/><br/>
                                            </div>
                                        </div>
                                    </div>                         
                                </Tab.Pane>

                                <Tab.Pane eventKey="second">
                                    <div  class="container"><br/>
                                        <div id="divAjuste" class="container" style={{ backgroundColor:'#E5E5E5'}}><br/>
                                            <div id="titulo" class="px-2">
                                                <h3>Mis Ajustes</h3>
                                            </div><br/>

                                            <form class="px-2">
                                                <div id="form-data">
                                                    <div class="row">
                                                        <div class="col">
                                                            <div class="form-group">
                                                                <label class="form-label">Nombres</label>
                                                                <input placeholder="Nombres" name="nombre" type="text" class="form-control" value="Carlos Eduardo"/>
                                                            </div>
                                                        </div>

                                                        <div class="col">
                                                            <div class="form-group">
                                                                <label class="form-label">Apellidos</label>
                                                                <input placeholder="Apellidos" name="apellido" type="text" class="form-control" value="Pumayalla Angeles"/>
                                                            </div>
                                                        </div>
                                                    </div><br/>

                                                    <div class="row">
                                                        <div class="col">
                                                            <div class="form-group">
                                                                <label class="form-label">Correo</label>
                                                                <input placeholder="Correo" name="correo" type="email" class="form-control" value="carlos@citeccall.com"/>
                                                                <small class="form-text">El email ingresado será usado para poder contactarte</small>
                                                            </div>
                                                        </div>

                                                        <div class="col">
                                                            <div class="form-group">
                                                                <label class="form-label">Contraseña</label>
                                                                <input placeholder="Contraseña" name="password" type="password" class="form-control" value="12345678"/>
                                                                <small class="form-text">Recuerda no compartir tu contraseña con nadie</small>
                                                            </div>
                                                        </div>                                                        
                                                    </div><br/>

                                                    <div class="row">
                                                        <div class="col">
                                                            <div class="form-group">
                                                                <label class="form-label">Celular</label>
                                                                <input placeholder="Numero" name="celular" type="number" class="form-control" value="987415421"/>
                                                            </div>
                                                        </div>

                                                        <div class="col">
                                                            <div class="form-group">
                                                                <label class="form-label">Teléfono</label>
                                                                <input placeholder="Telefono" name="telefono" type="number" class="form-control" value="5412354"/>
                                                            </div>
                                                        </div>                                                        
                                                    </div>
                                                </div><br/><br/>

                                                <div class="f-button text-center">
                                                    <button id="divBoton" type="submit" class="btn" style={{backgroundColor:'#696666', color:'#ffffff' }}>Guardar Cambios</button>                                                    
                                                </div><br/><br/>
                                            </form>
                                        </div>
                                    </div>                                
                                </Tab.Pane>
                            </Tab.Content>
                        </Col>
                    </Row>
                </Tab.Container>
              
          </div>
        )
    }
  }
  export default MiPerfil;