import React from 'react';
import '../../assets/styles/divs.css'
import TituloNueva from './tituloNueva/TituloNueva';
import FormNuevo from './formNuevo/FormNuevo';
import MenuLateral from '../acoples/menuLaterar/MenuLaterar';


class NuevaFicha extends React.Component {
    render(){
        return(
            <>
            <MenuLateral />
            <TituloNueva/> 
			<main role="main" class="">
                <div class="">
				    <FormNuevo />
                </div>	
	  		</main>
	  		</>
        )
    }
}
export default NuevaFicha;