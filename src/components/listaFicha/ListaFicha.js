import React from 'react';
import TituloLista from './tituloLista/TituloLista';
import Tabla from './tablaLista/Tabla';
import MenuLateral from '../acoples/menuLaterar/MenuLaterar';

class ListaFicha extends React.Component {
    render(){
        return(
            <>
            <MenuLateral />
            <TituloLista/> 
			<main role="main" class="">
                <div class="">
				    <Tabla />
                </div>	
	  		</main>
	  		</>
        )
    }
}
export default ListaFicha;